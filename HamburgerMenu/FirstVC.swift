//
//  HomeVC.swift
//  Dashboard
//
//  Created by Mikhail Baynov on 1/27/16.
//  Copyright © 2016 Mikhail Baynov. All rights reserved.
//

import UIKit

class FirstVC: UIViewController {
    
    override func viewDidLoad() {
        self.view.backgroundColor = UIColor.greenColor()
    }

    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        print("FirstVC touchesBegan!!!!!")
    }
}
