//
//  AppDelegate.swift
//  HamburgerMenu
//
//  Created by Mikhail Baynov on 2/5/16.
//  Copyright © 2016 Mikhail Baynov. All rights reserved.
//

import UIKit

///globals
let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate



@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    
    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        window = UIWindow(frame: UIScreen.mainScreen().bounds)
        if let window = window {
            window.backgroundColor = UIColor.whiteColor()
            window.rootViewController = NavigationVC()
            window.makeKeyAndVisible()
        }
        return true
    }
    
    
    
}
