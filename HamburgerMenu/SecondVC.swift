//
//  MapsVC.swift
//  Dashboard
//
//  Created by Mikhail Baynov on 1/27/16.
//  Copyright © 2016 Mikhail Baynov. All rights reserved.
//

import UIKit

class SecondVC: UIViewController {
    
    override func viewDidLoad() {
        self.view.backgroundColor = UIColor.magentaColor()
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        print("SecondVC touchesBegan!!!!!")
    }
}
