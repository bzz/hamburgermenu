import UIKit



class NavigationVC: UIViewController
{
    let menuOptions = ["FirstVC", "SecondVC", "ThirdVC", "more", "more", "and more"]
    let screenHeight = UIScreen.mainScreen().bounds.height
    let screenWidth = UIScreen.mainScreen().bounds.width
    let btnSide :CGFloat = 60;
    let menuWidth:CGFloat = UIScreen.mainScreen().bounds.width - 60
    
    var scrollView:UIScrollView!
    var mainViewBed = UIView()
    var selectedVC:UIViewController!
    
    
    
    override func viewDidLoad() {
        setupSlideMenu()
        setupPanels()
    }

/* It would be a 100-liner if it used storyboards! */
/* Begin of views setup  */
    
    func setupSlideMenu() {
        scrollView = UIScrollView(frame: self.view.bounds);
        scrollView.contentSize = CGSizeMake(screenWidth+menuWidth, screenHeight)
        scrollView.showsHorizontalScrollIndicator = false
        scrollView.bounces = false
        scrollView.delegate = self
        scrollView.tag = 777
        scrollView.scrollEnabled = false
        self.view.addSubview(scrollView)
        
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.itemSize = CGSizeMake(menuWidth, 80)
        flowLayout.scrollDirection = .Vertical
        
        let collectionView = UICollectionView(frame: CGRectMake(0, 100, menuWidth, screenHeight-100), collectionViewLayout: flowLayout)
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.registerClass(MenuCell.self, forCellWithReuseIdentifier: String(MenuCell))
        collectionView.backgroundColor = UIColor.whiteColor()
        
        let menuView: UIView! = UIView(frame: CGRectMake(scrollView.contentSize.width-menuWidth, 0, menuWidth, screenHeight))
        scrollView.addSubview(menuView)
        menuView.backgroundColor = UIColor.redColor()
        menuView.addSubview(collectionView)
    }
    
    func setupPanels() {
        mainViewBed = UIView(frame: CGRectMake(0, btnSide+20, screenWidth, screenHeight-btnSide*2-20))
        scrollView.addSubview(mainViewBed)
        navigate(nil, toVC: FirstVC())
        
        let topPanel = UIView(frame: CGRectMake(0, 20, screenWidth, btnSide))
        topPanel.backgroundColor = UIColor.grayColor()
        scrollView.addSubview(topPanel);
        scrollView.backgroundColor = topPanel.backgroundColor
        
        let notifButton = UIButton(frame: CGRectMake(screenWidth-btnSide, 0, btnSide, btnSide))
        notifButton.backgroundColor = UIColor.blackColor()
        notifButton.setTitle("NOTIF", forState: UIControlState.Normal)
        notifButton.addTarget(self, action: "notifButtonPressed", forControlEvents: UIControlEvents.TouchUpInside)
        topPanel.addSubview(notifButton)
        
        let bottomPanel = UIView(frame: CGRectMake(0, screenHeight-btnSide, screenWidth, btnSide))
        bottomPanel.backgroundColor = UIColor.grayColor()
        scrollView.addSubview(bottomPanel);
        
        let menuButton = UIButton(frame: CGRectMake(screenWidth-btnSide, 0, btnSide, btnSide))
        menuButton.backgroundColor = UIColor.blackColor()
        menuButton.setTitle("MENU", forState: UIControlState.Normal)
        menuButton.addTarget(self, action: "menuButtonPressed", forControlEvents: UIControlEvents.TouchUpInside)
        bottomPanel.addSubview(menuButton)
    }
    
/*  End of views setup  */
    
    
    func notifButtonPressed() {
        print("notifButtonPressed!!!")
    }
    
    func menuButtonPressed() {
        scrollView.contentOffset.x == 0  ? openMenu() : closeMenu()
    }
    
    func openMenu() {
        scrollView.setContentOffset(CGPoint(x: screenWidth-btnSide, y: 0), animated: true)
        scrollView.scrollEnabled = true;
    }
    func closeMenu(animated:Bool = true) {
        scrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: animated)
        scrollView.scrollEnabled = false;
    }
    
    
}





extension NavigationVC : UIScrollViewDelegate {
    
    func scrollViewWillBeginDragging(scrollView: UIScrollView) {
        if (scrollView.tag == 777) {
            scrollView.pagingEnabled = true
        }
    }
    
    func scrollViewDidEndDecelerating(scrollView: UIScrollView) {
        if (scrollView.tag == 777) {
            scrollView.contentOffset.x > screenWidth/2  ? openMenu() : closeMenu()
            scrollView.pagingEnabled = false
            
            scrollView.scrollEnabled = (scrollView.contentOffset.x == 0) ? false : true
        }
    }
    
    func navigate(fromVC: UIViewController?, toVC: UIViewController?) -> Bool {
        if fromVC == toVC {return false}
        
        if (fromVC != nil) {
            fromVC!.willMoveToParentViewController(nil)
            fromVC!.view.removeFromSuperview()
            fromVC!.removeFromParentViewController()
        }
        if (toVC != nil) {
            self.addChildViewController(toVC!)
            toVC!.view.frame = mainViewBed.bounds;               // <=== ISSUE: This line fails set child viewcontroller's view frame
            mainViewBed.addSubview(toVC!.view)
            toVC!.didMoveToParentViewController(self)
            selectedVC = toVC
        }
        return true
    }
}


extension NavigationVC: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {

    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return menuOptions.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(String(MenuCell), forIndexPath: indexPath) as! MenuCell
        cell.backgroundColor = UIColor.orangeColor()
        cell.label.text = menuOptions[indexPath.row]
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        var toVC: UIViewController!;
        switch indexPath.row {
        case 0:
            toVC = FirstVC();
        case 1:
            toVC = SecondVC();
        case 2:
            toVC = ThirdVC()
        default: break }
        if toVC != nil {
            if navigate(selectedVC, toVC: toVC) {
                closeMenu()
            }
        }
    }
}




class MenuCell: UICollectionViewCell {
    var label = UILabel()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        didInit()
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        didInit()
    }
    
    func didInit()
    {
        label = UILabel(frame: bounds);
        self.addSubview(label)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        label.text = nil
    }
}



